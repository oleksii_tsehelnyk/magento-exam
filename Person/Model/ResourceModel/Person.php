<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Tsehelnyk\Person\Api\Data\PersonInterface;

/**
 * Class Person
 * @package Tsehelnyk\Person\Model\ResourceModel
 */
class Person extends AbstractDb
{
    const PERSON_TABLE = 'persons';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(self::PERSON_TABLE, PersonInterface::ID_PERSON);
    }

}
