<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Model\ResourceModel\Person;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Model\PersonModel;
use Tsehelnyk\Person\Model\ResourceModel\Person;

/**
 * Class Collection
 * @package Tsehelnyk\Person\Model\ResourceModel\Person
 */
class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected $_idFieldName = PersonInterface::ID_PERSON;

    /**
     * {@inheritdoc }
     */
    public function _construct()
    {
        $this->_init(PersonModel::class, Person::class);
    }


}
