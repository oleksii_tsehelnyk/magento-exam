<?php

namespace Tsehelnyk\Person\Model;

use Magento\Framework\Model\AbstractModel;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Model\ResourceModel\Person;

/**
 * Class PersonModel
 * @package Tsehelnyk\Person\Model
 */
class PersonModel extends AbstractModel implements PersonInterface
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(Person::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID_PERSON);
    }

    /**
     * @inheritDoc
     */
    public function getSurname(): string
    {
        return $this->getData(self::SURNAME);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function getAge(): int
    {
        return $this->getData(self::AGE);
    }

    /**
     * @inheritDoc
     */
    public function getSex(): string
    {
        return $this->getData(self::SEX);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheriDoc
     */
    public function getExtensionAttributes()
    {
        return $this->getData(self::EXTENSION_ATTRIBUTES_KEY);
    }

    /**
     * @inheritDoc
     */
    public function setSurname($surname): PersonInterface
    {
        return $this->setData(self::SURNAME, $surname);
    }

    /**
     * @inheritDoc
     */
    public function setName($name): PersonInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function setAge($age): PersonInterface
    {
        return $this->setData(self::AGE, $age);
    }

    /**
     * @inheritDoc
     */
    public function setSex($sex): PersonInterface
    {
        return $this->setData(self::SEX, $sex);
    }

    /**
     * @inheritDoc
     */
    public function setContent($content): PersonInterface
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt): PersonInterface
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes($attributes): PersonInterface
    {
        return $this->setData(self::EXTENSION_ATTRIBUTES_KEY, $attributes);
    }
}
