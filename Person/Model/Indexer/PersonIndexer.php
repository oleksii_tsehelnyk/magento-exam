<?php

namespace Tsehelnyk\Person\Model\Indexer;

use Exception;
use Magento\Framework\Indexer\ActionInterface as IndexerActionInterface;
use Magento\Framework\Mview\ActionInterface as MviewActionInterface;
use Tsehelnyk\Person\Logger\PersonLogger;

class PersonIndexer implements IndexerActionInterface, MviewActionInterface
{
    /**
     * @param PersonLogger $logger
     */
    public function __construct(
        private readonly PersonLogger $logger
    ) {
    }

    /**
     * @return void
     */
    public function executeFull(): void
    {
        try {
            $this->logger->info("Full index was executed");
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param array $ids
     * @return void
     */
    public function executeList(array $ids): void
    {
        try {
            $this->logger->info("List of indexes were executed");
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return void
     */
    public function executeRow($id): void
    {
        try {
            $this->logger->info("Row of index was executed");
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    /**
     * @param $ids
     * @return void
     */
    public function execute($ids): void
    {
        try {
            $this->logger->info("Index was executed");
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
