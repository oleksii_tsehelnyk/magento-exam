<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Tsehelnyk\Person\Model\ResourceModel\Person\CollectionFactory;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;
use Tsehelnyk\Person\Model\ResourceModel\Person;

/**
 * Class PersonRepository
 * @package Tsehelnyk\Person\Model
 */
class PersonRepository implements PersonRepositoryInterface
{
    /**
     * @var PersonModelFactory
     */
    private PersonModelFactory $modelFactory;

    /**
     * @var Person
     */
    private Person $resource;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private SearchResultsInterfaceFactory $searchResultsFactory;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessor;

    /**
     * @param PersonModelFactory $modelFactory
     * @param Person $resource
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        PersonModelFactory $modelFactory,
        Person $resource,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->modelFactory = $modelFactory;
        $this->resource = $resource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     * @throws CouldNotSaveException
     */
    public function save(PersonInterface $person): PersonInterface
    {
        try {
            /** @var PersonModel|PersonInterface */
            $this->resource->save($person);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $person;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getById(int $personId): PersonInterface
    {
        $person = $this->modelFactory->create();
        $this->resource->load($person, $personId);
        if(!$person->getId()) {
            throw new NoSuchEntityException(__('Person with id `%1` does not exist.' . $personId, $personId));
        }

        return $person;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }


    /**
     * @inheritDoc
     * @throws CouldNotDeleteException
     */
    public function delete(PersonInterface $person): bool
    {
        try {
            $this->resource->delete($person);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $personId): bool
    {
        try {
            $delete = $this->delete($this->getById($personId));
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return $delete;
    }

}
