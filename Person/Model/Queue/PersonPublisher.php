<?php

namespace Tsehelnyk\Person\Model\Queue;

use Magento\Framework\MessageQueue\PublisherInterface;
use Tsehelnyk\Person\Api\Data\PersonInterfaceFactory;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;

class PersonPublisher
{
    /**
     * @var string
     */
    public const TOPIC_NAME = 'person.update';

    /**
     * @param PersonInterfaceFactory $personInterfaceFactory
     * @param PersonRepositoryInterface $personRepository
     * @param PublisherInterface $publisher
     */
    public function __construct(
        private readonly PersonInterfaceFactory $personInterfaceFactory,
        private readonly PersonRepositoryInterface $personRepository,
        private readonly PublisherInterface $publisher
    ) {
    }

    /**
     * @param array $personData
     * @return void
     */
    public function execute(array $personData): void
    {
        $person = $this->personInterfaceFactory->create();
        $person->setSurname($personData['surname']);
        $person->setName($personData['name']);
        $person->setAge($personData['age']);
        $person->setSex($personData['sex']);
        $person->setContent($personData['content']);
        $person->setCreatedAt(date('Y-m-d H:m:s'));
        $person = $this->personRepository->save($person);

        $this->publisher->publish(self::TOPIC_NAME, $person->getId());
    }
}
