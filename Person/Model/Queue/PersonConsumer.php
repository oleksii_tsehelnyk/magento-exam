<?php

namespace Tsehelnyk\Person\Model\Queue;

use Exception;
use Psr\Log\LoggerInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;

/**
 * Class PersonConsumer
 * Consumer for person.update queue
 */
class PersonConsumer
{
    /**
     * @param PersonRepositoryInterface $personRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly PersonRepositoryInterface $personRepository,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @param string $personId
     * @return void
     * @throws Exception
     */
    public function process(string $personId): void
    {
        try {
            $person = $this->personRepository->getById($personId);
            $person->setAge(50);
            $this->personRepository->save($person);
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw new Exception(
                "Message of the 'person.update' query was not executed successfully. Original error: " . $exception->getMessage()
            );
        }
    }
}
