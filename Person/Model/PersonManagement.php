<?php

namespace Tsehelnyk\Person\Model;

use Tsehelnyk\Person\Api\PersonManagementInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;

class PersonManagement implements PersonManagementInterface
{
    /**
     * @var PersonRepositoryInterface
     */
    private PersonRepositoryInterface $personRepository;

    /**
     * @param PersonRepositoryInterface $personRepository
     */
    public function __construct(PersonRepositoryInterface $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getPerson(int $param): \Tsehelnyk\Person\Api\Data\PersonInterface
    {
        return $this->personRepository->getById($param);
    }

    /**
     * {@inheritdoc}
     */
    public function createPerson(\Tsehelnyk\Person\Api\Data\PersonInterface $person): \Tsehelnyk\Person\Api\Data\PersonInterface
    {
        return $this->personRepository->save($person);
    }
}
