<?php

namespace Tsehelnyk\Person\Model\Config;

use Magento\Framework\Config\ConverterInterface;

class Converter implements ConverterInterface
{
    /**
     * {@inheritdoc}
     */
    public function convert($source): array
    {
        $list = $source->getElementsByTagName('persons_list');
        $personInfo = [];
        $iterator = 0;
        foreach ($list as $persons) {
            foreach ($persons->childNodes as $person) {
                if (isset($person->localName)) {
                    foreach ($person->childNodes as $attribute) {
                        if (isset($attribute->localName)) {
                            $personInfo[$iterator][$attribute->localName] = $attribute->textContent;
                        }
                    }
                    $iterator++;
                }
            }
        }
        return ['persons' => $personInfo];
    }
}
