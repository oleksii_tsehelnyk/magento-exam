<?php

namespace Tsehelnyk\Person\Model\Config;

use Magento\Framework\Config\SchemaLocatorInterface;
use Magento\Framework\Module\Dir;
use Magento\Framework\Module\Dir\Reader;

class SchemaLocator implements SchemaLocatorInterface
{
    /**
     * XML schema for config file.
     */
    const CONFIG_FILE_SCHEMA = 'persons_list.xml';

    /**
     * Path to corresponding XSD file with validation rules for merged config
     *
     * @var string|null
     */
    protected ?string $schema = 'persons_list.xsd';

    /**
     * Path to corresponding XSD file with validation rules for separate config files
     * @var string|null
     */
    protected ?string $perFileSchema = 'persons_list.xsd';

    /**
     * @param Reader $moduleReader
     */
    public function __construct(Reader $moduleReader)
    {
        $configDir = $moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Tsehelnyk_Person');
        $this->schema = $configDir . DIRECTORY_SEPARATOR . $this->schema;
        $this->perFileSchema = $configDir . DIRECTORY_SEPARATOR . $this->perFileSchema;
    }

    /**
     * {@inheritdoc}
     */
    public function getSchema(): ?string
    {
        return $this->schema;
    }

    /**
     * {@inheritdoc }
     */
    public function getPerFileSchema(): ?string
    {
        return $this->perFileSchema;
    }
}
