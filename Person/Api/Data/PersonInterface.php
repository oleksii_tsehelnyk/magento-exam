<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface PersonInterface
 * @package Tsehelnyk\Person\Api\Data
 */
interface PersonInterface extends ExtensibleDataInterface
{
    const ID_PERSON = 'id_person';
    const SURNAME = 'surname';
    const NAME = 'name';
    const AGE = 'age';
    const SEX = 'sex';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getSurname(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getAge(): int;

    /**
     * @return string
     */
    public function getSex(): string;

    /**
     * @return ?string
     */
    public function getContent(): ?string;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @return \Tsehelnyk\Person\Api\Data\PersonExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * @param $surname
     * @return PersonInterface
     */
    public function setSurname($surname): PersonInterface;

    /**
     * @param $name
     * @return PersonInterface
     */
    public function setName($name): PersonInterface;

    /**
     * @param $age
     * @return PersonInterface
     */
    public function setAge($age): PersonInterface;

    /**
     * @param $sex
     * @return PersonInterface
     */
    public function setSex($sex): PersonInterface;

    /**
     * @param string|null $content
     * @return PersonInterface
     */
    public function setContent(string|null $content): PersonInterface;

    /**
     * @param $createdAt
     * @return PersonInterface
     */
    public function setCreatedAt($createdAt): PersonInterface;

    /**
     * @param  \Tsehelnyk\Person\Api\Data\PersonExtensionInterface $attributes
     * @return $this
     */
    public function setExtensionAttributes(\Tsehelnyk\Person\Api\Data\PersonExtensionInterface $attributes);
}
