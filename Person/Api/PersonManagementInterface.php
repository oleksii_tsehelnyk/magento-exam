<?php

namespace Tsehelnyk\Person\Api;

interface PersonManagementInterface
{
    /**
     * @param int $param
     * @return \Tsehelnyk\Person\Api\Data\PersonInterface
     */
    public function getPerson(int $param): \Tsehelnyk\Person\Api\Data\PersonInterface;

    /**
     * @param \Tsehelnyk\Person\Api\Data\PersonInterface $person
     * @return \Tsehelnyk\Person\Api\Data\PersonInterface
     */
    public function createPerson(\Tsehelnyk\Person\Api\Data\PersonInterface $person): \Tsehelnyk\Person\Api\Data\PersonInterface;

}
