<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Tsehelnyk\Person\Api\Data\PersonInterface;

/**
 * Interface PersonRepositoryInterface
 * @package Tsehelnyk\Person\Api
 */
interface PersonRepositoryInterface
{
    /**
     * @param PersonInterface $person
     * @return PersonInterface
     */
    public function save(PersonInterface $person): PersonInterface;

    /**
     * @param int $personId
     * @return PersonInterface
     */
    public function getById(int $personId): PersonInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResults
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults;

    /**
     * @param PersonInterface $person
     * @return bool
     */
    public function delete(PersonInterface $person): bool;

    /**
     * @param int $personId
     * @return bool
     */
    public function deleteById(int $personId): bool;

}
