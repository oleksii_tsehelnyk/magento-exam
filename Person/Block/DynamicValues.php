<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Block;

use Magento\Cms\Model\BlockFactory;
use Magento\Framework\View\Element\Template;

/**
 * Class DynamicValues
 * @package Tsehelnyk\Person\Block
 */
class DynamicValues extends Template
{
    /**
     * @var BlockFactory
     */
    private BlockFactory $blockFactory;

    /**
     * @var \Magento\Framework\Filter\Template
     */
    private \Magento\Framework\Filter\Template $template;

    /**
     * @param Template\Context $context
     * @param BlockFactory $blockFactory
     * @param \Magento\Framework\Filter\Template $template
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        BlockFactory $blockFactory,
        \Magento\Framework\Filter\Template $template,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->blockFactory = $blockFactory;
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getBlock(): string
    {
            $block = $this->blockFactory->create();
            $block->load('test-block');
            $array['surname'] = 'Zaharov';
            $array['name'] = 'Misha';
            $array['age'] = '25';
            $array['sex'] = 'Female';
            $this->template->setVariables($array);
            return $this->template->filter($block->getContent());
    }
}
