<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Block;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Tsehelnyk\Person\Model\PersonRepository;

/**
 * Class Main
 * @package Tsehelnyk\Person\Block
 */
class Main extends Template
{
    /**
     * @var PersonRepository
     */
    private PersonRepository $personRepository;

    /**
     * @var SearchCriteriaInterface
     */
    private SearchCriteriaInterface $searchCriteria;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @param Template\Context $context
     * @param PersonRepository $personRepository
     * @param SearchCriteriaInterface $searchCriteria
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        PersonRepository $personRepository,
        SearchCriteriaInterface $searchCriteria,
        StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->personRepository = $personRepository;
        $this->searchCriteria = $searchCriteria;
        $this->storeManager = $storeManager;
    }

    /**
     * @return SearchResults
     */
    public function getItems(): SearchResults
    {
        $searchResult = "";
        try {
            $searchResult = $this->personRepository->getList($this->searchCriteria);
        } catch (\Exception $exception) {
            $this->_logger->critical($exception->getMessage());
        }
        return $searchResult;
    }

    /**
     * @return string
     */
    public function getDomen(): string
    {
        $url = "";
        try {
            $url = $this->storeManager->getStore()->getBaseUrl();
        } catch (\Exception $exception) {
            $this->_logger->critical($exception->getMessage());
        }
        return $url;
    }

}
