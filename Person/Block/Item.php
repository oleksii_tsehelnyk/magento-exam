<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Block;

use Magento\Framework\View\Element\Template;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Model\PersonRepository;

/**
 * Class Item
 * @package Tsehelnyk\Person\Block
 */
class Item extends Template
{
    /**
     * @var PersonRepository
     */
    private PersonRepository $personRepository;

    /**
     * @param Template\Context $context
     * @param PersonRepository $personRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        PersonRepository $personRepository,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->personRepository = $personRepository;
    }

    /**
     * @return PersonInterface
     */
    public function getItem(): PersonInterface
    {
        $person = "";
        try {
            $person = $this->personRepository->getById($this->getData('id'));
        } catch (\Exception $exception) {
            $this->_logger->critical($exception->getMessage());
        }
        return $person;
    }

}
