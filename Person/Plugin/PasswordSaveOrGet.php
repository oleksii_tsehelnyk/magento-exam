<?php

namespace Tsehelnyk\Person\Plugin;

use Tsehelnyk\Person\Api\Data\PersonExtensionInterfaceFactory;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;

class PasswordSaveOrGet
{

    /**
     * @var PersonExtensionInterfaceFactory
     */
    private PersonExtensionInterfaceFactory $personExtension;

    /**
     * @param PersonExtensionInterfaceFactory $personExtension
     */
    public function __construct(PersonExtensionInterfaceFactory $personExtension)
    {
        $this->personExtension = $personExtension;
    }

    public function afterGetById(PersonRepositoryInterface $subject, PersonInterface $result)
    {
        $extensionAttributes = $result->getExtensionAttributes();
        $extensions = $extensionAttributes ? $extensionAttributes : $this->personExtension->create();
        $extensions->setPassportCode('12345');
        $result->setExtensionAttributes($extensions);

        return $result;
    }

    public function beforeSave(
        PersonRepositoryInterface $subject,
        PersonInterface $person
    ) {
        return $person;
    }
}
