<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Controller\Adminhtml\Index;

use Exception;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;
use Tsehelnyk\Person\Model\PersonModelFactory;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\Request\Http as HttpRequest;
use Zend_Filter_Exception;
use Zend_Filter_Interface;

/**
 * Class Save
 * @package Tsehelnyk\Person\Controller\Adminhtml\Index
 */
class Save extends BackendAction implements HttpPostActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_Person::person_save';

    /**
     * @var DataPersistorInterface
     */
    private DataPersistorInterface $dataPersist;

    /**
     * @var PersonRepositoryInterface
     */
    private PersonRepositoryInterface $personRepository;

    /**
     * @var PersonInterface
     */
    private $personFactory;

    /**
     * @var Zend_Filter_Interface
     */
    private Zend_Filter_Interface $templateProcessor;

    /**
     * @param Context $context
     * @param PersonRepositoryInterface $personRepository
     * @param PersonModelFactory $personFactory
     * @param DataPersistorInterface $dataPersist
     * @param Zend_Filter_Interface $templateProcessor
     */
    public function __construct(
        Context $context,
        PersonRepositoryInterface $personRepository,
        PersonModelFactory $personFactory,
        DataPersistorInterface $dataPersist,
        Zend_Filter_Interface $templateProcessor
    ) {
        $this->dataPersist = $dataPersist;
        $this->personRepository = $personRepository;
        $this->personFactory = $personFactory;
        parent::__construct($context);
        $this->templateProcessor = $templateProcessor;
    }

    /**
     * @return Redirect
     * @throws Zend_Filter_Exception
     */
    public function execute(): Redirect
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var HttpRequest $request */
        $request = $this->getRequest();
        $data = $request->getPostValue();
        if ($data) {
                $data['content'] = $this->templateProcessor->filter($data['content']);
                $id = $this->getRequest()->getParam(PersonInterface::ID_PERSON);
                if (empty($data[PersonInterface::ID_PERSON])) {
                    $data[PersonInterface::ID_PERSON] = null;
                }

                if ($id) {
                    $model = $this->personRepository->getById((int)$id);
                } else {
                    /** @var PersonInterface $model */
                    $model = $this->personFactory->create();
                }
                $model->setData($data);

                try {
                    $this->personRepository->save($model);
                    $this->messageManager->addSuccessMessage(__('You saved the row.'));
                    $this->dataPersist->clear('row');
                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', [PersonInterface::ID_PERSON => $model->getId()]);
                    }

                    return $resultRedirect->setPath('*/*/');
                } catch (Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the row.'));
                }

                $this->dataPersist->set('vendor', $data);
                return $resultRedirect->setPath('*/*/edit', [PersonInterface::ID_PERSON => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
