<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Tsehelnyk\Person\Api\Data\PersonInterface;
use Tsehelnyk\Person\Api\PersonRepositoryInterface;

/**
 * Class Delete
 * @package Tsehelnyk\Person\Controller\Adminhtml\Index
 */
class Delete extends BackendAction implements HttpGetActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_Person::person_delete';

    /**
     * @var PersonRepositoryInterface
     */
    private PersonRepositoryInterface $personRepository;

    /**
     * @var Context
     */
    private Context $context;

    /**
     * @param Context $context
     * @param PersonRepositoryInterface $personRepository
     * @param DataPersistorInterface $dataPersist
     */
    public function __construct(
        Context $context,
        PersonRepositoryInterface $personRepository,
        DataPersistorInterface $dataPersist
    ) {
        $this->dataPersist = $dataPersist;
        parent::__construct($context);
        $this->personRepository = $personRepository;
        $this->context = $context;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam(PersonInterface::ID_PERSON);

        try {
            $this->personRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('You deleted the row'));
            $this->dataPersist->clear('row');
            return $resultRedirect->setPath('*/*/');
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting the row.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
