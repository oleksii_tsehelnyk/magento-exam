<?php

namespace Tsehelnyk\Person\Controller\Create;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Tsehelnyk\Person\Model\Queue\PersonConsumer;
use Tsehelnyk\Person\Model\Queue\PersonPublisher;

class Post implements ActionInterface
{
    /**
     * @param RedirectFactory $redirectFactory
     * @param PersonPublisher $personPublisher
     * @param Http $http
     */
    public function __construct(
        private readonly RedirectFactory $redirectFactory,
        private readonly PersonPublisher $personPublisher,
        private readonly Http $http
    ) {
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $data = $this->http->getParams();

        $this->personPublisher->execute($data);
        $result = $this->redirectFactory->create();
        return $result->setPath('person/create/');
    }
}
