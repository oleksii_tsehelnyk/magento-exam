<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Controller\Index;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Tsehelnyk\Person\Model\Config\Data;

/**
 * Class Item
 * @package Tsehelnyk\Person\Controller\Index
 */
class Item implements ActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    /**
     * @var Http
     */
    private Http $http;

    /**
     * @var Data
     */
    private Data $data;

    /**
     * @param PageFactory $pageFactory
     * @param Http $http
     * @param Data $data
     */
    public function __construct(PageFactory $pageFactory, Http $http, Data $data)
    {
        $this->pageFactory = $pageFactory;
        $this->http = $http;
        $this->data = $data;
    }

    /**
     * @return Page|ResultInterface
     */
    public function execute()
    {
        $configs = $this->data->get('persons');
        $page = $this->pageFactory->create();
        $block = $page->getLayout()->getBlock('item.block');
        $data = (int)$this->http->getParam('id');
        if($data) {
            $block->setData('id', $data);
        }
        return $page;
    }

}
