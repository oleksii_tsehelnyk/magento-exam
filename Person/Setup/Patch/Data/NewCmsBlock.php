<?php
declare(strict_types=1);

namespace Tsehelnyk\Person\Setup\Patch\Data;

use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class NewCmsBlock
 * @package Tsehelnyk\Person\Setup\Patch\Data
 */
class NewCmsBlock implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var BlockFactory
     */
    private BlockFactory $blockFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param DateTime $dateTime
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        DateTime $dateTime,
        BlockFactory $blockFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->dateTime = $dateTime;
        $this->blockFactory = $blockFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        echo 'Tsehelnyk_Person:CreateNewCmsBlock:startSetup' . "\r\n";


        $testBlock = [
            'title' => 'Test Cms Block',
            'identifier' => 'test-block',
            'is_active' => 1,
            'content' => '<style>#html-body [data-pb-style=H5ICSTJ],#html-body [data-pb-style=NOH0RGM]{border-width:1px}#html-body [data-pb-style=T65U5SP]{text-align:left}#html-body [data-pb-style=KLK7TQL]{border-width:1px;min-height:300px}#html-body [data-pb-style=CYWNWFA],#html-body [data-pb-style=WW0G7D4],#html-body [data-pb-style=YC1WHB2]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=UI5228L]{height:300px}#html-body [data-pb-style=A64SLN4]{border-style:none}#html-body [data-pb-style=DXJ4V57],#html-body [data-pb-style=GG355XY]{max-width:100%;height:auto}#html-body [data-pb-style=E1SO0LU]{display:inline-block}#html-body [data-pb-style=P7SWK8D]{text-align:center}@media only screen and (max-width: 768px) { #html-body [data-pb-style=A64SLN4]{border-style:none} }</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="WW0G7D4"><div data-content-type="text" data-appearance="default" data-element="main"><p>{{var surname}}</p><p>{{var name}}</p><p>{{var age}}</p><p>{{var sex}}</p></div><div data-content-type="buttons" data-appearance="inline" data-same-width="false" data-element="main"><div data-content-type="button-item" data-appearance="default" data-element="main" data-pb-style="E1SO0LU"><div class="pagebuilder-button-primary" data-element="empty_link" data-pb-style="P7SWK8D"><span data-element="link_text">Test Button</span></div></div></div></div></div><div class="tab-align-left" data-content-type="tabs" data-appearance="default" data-active-tab="" data-element="main"><ul role="tablist" class="tabs-navigation" data-element="navigation" data-pb-style="T65U5SP"><li role="tab" class="tab-header" data-element="headers" data-pb-style="NOH0RGM"><a href="#L076S5C" class="tab-title"><span class="tab-title">Tab 1</span></a></li><li role="tab" class="tab-header" data-element="headers" data-pb-style="H5ICSTJ"><a href="#BH1W1XJ" class="tab-title"><span class="tab-title">Tab 2</span></a></li></ul><div class="tabs-content" data-element="content" data-pb-style="KLK7TQL"><div data-content-type="tab-item" data-appearance="default" data-tab-name="Tab 1" data-background-images="{}" data-element="main" id="L076S5C" data-pb-style="YC1WHB2"><figure data-content-type="image" data-appearance="full-width" data-element="main" data-pb-style="A64SLN4"><img class="pagebuilder-mobile-hidden" src="https://magento2.dev/media/wysiwyg/og-photo-200120210858_1.jpg" alt="" title="" data-element="desktop_image" data-pb-style="GG355XY"><img class="pagebuilder-mobile-only" src="https://magento2.dev/media/wysiwyg/og-photo-200120210858_1.jpg" alt="" title="" data-element="mobile_image" data-pb-style="DXJ4V57"></figure></div><div data-content-type="tab-item" data-appearance="default" data-tab-name="Tab 2" data-background-images="{}" data-element="main" id="BH1W1XJ" data-pb-style="CYWNWFA"><div data-content-type="map" data-appearance="default" data-show-controls="true" data-locations="[]" data-element="main" data-pb-style="UI5228L"></div></div></div></div>',
            'creation_at' => $this->dateTime->gmtDate(),
            'updated_at' => $this->dateTime->gmtDate()
        ];

        $this->blockFactory->create()->setData($testBlock)->save();

        $this->moduleDataSetup->endSetup();

        echo 'Tsehelnyk_Person:CreateNewCmsBlock:endSetup' . "\r\n";
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}
