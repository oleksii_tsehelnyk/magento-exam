<?php

namespace Tsehelnyk\CheckoutStep\Api;

interface ValidateManagementInterface
{
    /**
     * @param int $id
     * @param int $age
     * @return void
     */
    public function validateAge(int $id, int $age);

    /**
     * @param int $id
     * @return string
     */
    public function checkStatus(int $id): string;
}
