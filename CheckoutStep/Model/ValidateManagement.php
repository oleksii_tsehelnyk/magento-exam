<?php

namespace Tsehelnyk\CheckoutStep\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Psr\Log\LoggerInterface;
use Tsehelnyk\CheckoutStep\Api\ValidateManagementInterface;
use Zend_Http_Client;

class ValidateManagement implements ValidateManagementInterface
{
    /**
     * @var CurlFactory
     */
    private CurlFactory $curlFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param CurlFactory $curlFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        CurlFactory                 $curlFactory,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface             $logger
    )
    {
        $this->curlFactory = $curlFactory;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAge(int $id, int $age)
    {
        $url = 'https://webhook.site/4b913f9b-f865-470b-8ed1-381c0f99f956?age='.$age.'&id='.$id;

        $client = $this->curlFactory->create();
        $client->addOption(CURLOPT_CONNECTTIMEOUT, 2);
        $client->addOption(CURLOPT_TIMEOUT, 3);
        $client->write(
            Zend_Http_Client::GET,
            $url,
            '1.1',
            ['Content-type: application/json']
        );

        $client->read();
        $client->close();
    }

    /**
     * {@inheritdoc}
     */
    public function checkStatus(int $id): string
    {
        if(!isset($id)) {
            return 'no';
        }
        $status = '';
        try {
            $customer = $this->customerRepository->getById($id);
            $status = $customer->getCustomAttribute('age_verification')->getValue();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
        return $status == 'Tak' ? 'okay' : 'no';
    }
}
