var config = {
    'config': {
        'mixins': {
            'Magento_Checkout/js/view/shipping': {
                'Tsehelnyk_CheckoutStep/js/view/shipping-payment-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'Tsehelnyk_CheckoutStep/js/view/shipping-payment-mixin': true
            }
        }
    }
}
