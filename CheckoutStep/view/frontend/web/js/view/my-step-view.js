define([
    'jquery',
    'ko',
    'uiComponent',
    'underscore',
    'Magento_Checkout/js/model/step-navigator'
], function ($, ko, Component, _, stepNavigator) {
    'use strict';

    /**
     * mystep - is the name of the component's .html template,
     * <Vendor>_<Module>  - is the name of your module directory.
     */
    let i = 0;
    return Component.extend({
        defaults: {
            template: 'Tsehelnyk_CheckoutStep/mystep'
        },

        // add here your logic to display step,
        isVisible: ko.observable(true),

        /**
         * @returns {*}
         */
        initialize: function () {
            this._super();

            // register your step
            stepNavigator.registerStep(
                // step code will be used as step content id in the component template
                'step_code',
                // step alias
                null,
                // step title value
                'Step Title',
                // observable property with logic when display step or hide step
                this.isVisible,

                _.bind(this.navigate, this),

                /**
                 * sort order value
                 * 'sort order value' < 10: step displays before shipping step;
                 * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                 * 'sort order value' > 20 : step displays after payment step
                 */
                9
            );

            return this;
        },

        /**
         * The navigate() method is responsible for navigation between checkout steps
         * during checkout. You can add custom logic, for example some conditions
         * for switching to your custom step
         * When the user navigates to the custom step via url anchor or back button we_must show step manually here
         */
        navigate: function () {
            this.isVisible(true);
        },

        /**
         * @returns void
         */
        navigateToNextStep: function () {
            stepNavigator.next();
        },

        validate: function() {
            // trigger form validation
            this.source.set('params.invalid', false);
            this.source.trigger('customCheckoutForm.data.validate');

            // verify that form data is valid
            if (!this.source.get('params.invalid')) {
                // data is retrieved from data provider by value of the customScope property
                var formData = this.source.get('customCheckoutForm');
                // do something with form data
                console.dir(formData);
            }
        },

        sendAjax: function () {
            $.ajax({
                url: '/rest/all/V1/tsehelnyk-checkoutstep/validate',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    id: document.getElementById('id_blak').value,
                    age: document.getElementById('age').value,
                })
            })
        },

        onSubmit: function() {
            if(i === 0){
                if(document.getElementById('age').value.length === 0){
                    error.textContent = "Please enter a valid number"
                    error.style.color = "red"
                } else {
                    error.textContent = ""
                    this.sendAjax();
                    i++;
                    document.getElementById('checkButton').innerHTML = "Refresh";
                }
            } else {
                $.ajax({
                    url: '/rest/all/V1/tsehelnyk-checkoutstep/check',
                    type: 'post',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        id: document.getElementById('id_blak').value
                    }),
                    success: function (data, status){
                        if(data === 'no'){
                            alert(data);
                        }
                        else if(data === 'okay') {
                            document.getElementById('button_to_stop').removeAttribute('disabled')
                        }
                    }
                })
            }
        }
    });
});
