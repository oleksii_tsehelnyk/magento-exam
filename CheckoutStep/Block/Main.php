<?php

namespace Tsehelnyk\CheckoutStep\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;

class Main extends Template
{
    /**
     * @var Session
     */
    private Session $session;

    /**
     * @param Template\Context $context
     * @param Session $session
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session          $session,
        array            $data = []
    )
    {
        parent::__construct($context, $data);
        $this->session = $session;
    }

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int
    {
        return $this->session->getCustomerId();
    }
}
