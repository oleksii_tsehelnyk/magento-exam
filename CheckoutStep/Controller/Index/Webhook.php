<?php

namespace Tsehelnyk\CheckoutStep\Controller\Index;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Webhook implements ActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var Http
     */
    private Http $http;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param PageFactory $pageFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param Http $http
     * @param LoggerInterface $logger
     */
    public function __construct(
        PageFactory                 $pageFactory,
        CustomerRepositoryInterface $customerRepository,
        Http                        $http,
        LoggerInterface             $logger
    )
    {
        $this->pageFactory = $pageFactory;
        $this->customerRepository = $customerRepository;
        $this->http = $http;
        $this->logger = $logger;
    }

    /**
     * @return Page|ResultInterface
     */
    public function execute()
    {
        $age = $this->http->getParam('age');
        $id = $this->http->getParam('id');
        if(isset($age) && isset($id))
        {
            try {
                $customer = $this->customerRepository->getById($id);

                if($age > 17){
                    $customer->setCustomAttribute('age_verification', 'Tak');
                }
                else {
                    $customer->setCustomAttribute('age_verification', 'Ni');
                }

                $this->customerRepository->save($customer);
            } catch (\Exception $exception) {
                $this->logger->critical($exception->getMessage());
            }
        }
        return $this->pageFactory->create();
    }
}
